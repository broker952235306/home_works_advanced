# Создайте функцию-генератор чисел Фибоначчи. Примените к ней декоратор,
# который будет оставлять в последовательности только чётные числа.

def decorator(fn):
    def dec(*args, **kwargs):
        print('1')
        fn(*args, **kwargs)
        if fn % 2 != 0:
            print(fn)
    return dec

@decorator
def func():
    def fibonacci_1(n):
        a, b = 0, 1
        while a < n:
            yield a
            a, b = b, a + b

    print(list(fibonacci_1(10)))

func()
