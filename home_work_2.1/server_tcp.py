import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as massager:
    massager.bind(('127.0.0.1', 5556))
    while True:
        massager.listen()
        conn, addr = massager.accept()
        massage = conn.recv(1024)
        conn.close()
        with conn:
            print(addr)
        print(massage.decode('utf-8'))
