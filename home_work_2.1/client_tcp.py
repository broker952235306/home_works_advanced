import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as r:
    r.connect(('127.0.0.1', 5556))
    r.send(b'massage from tcp client!')
