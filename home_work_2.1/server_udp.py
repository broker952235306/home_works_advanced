# Создайте UDP сервер, который ожидает сообщения о новых устройствах в сети. Он принимает сообщения определенного формата,
# в котором будет идентификатор устройства и печатает новые подключения в консоль. Создайте UDP клиента, который будет
# отправлять уникальный идентификатор устройства на сервер, уведомляя о своем присутствии.

import socket

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as f:
    f.bind(('127.0.0.1', 9874))
    a = f.recv(1024)
    print(a.decode('utf-8'))
