import os
import socket

unix_sock_name = 'unix.sock'
with socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM) as g:
    g.bind(unix_sock_name)
    m = g.recv(1024)
    print(m.decode('utf-8'))
